import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { JumbotronComponent } from './components/jumbotron/jumbotron.component';
import { OurSpecialtyComponent } from './components/our-specialty/our-specialty.component';
import { PanelComponent } from './helpers/panel/panel.component';
import { GoogleMapsComponent } from './components/google-maps/google-maps.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    JumbotronComponent,
    OurSpecialtyComponent,
    PanelComponent,
    GoogleMapsComponent,
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyALZ71Y84f1j9Lv_i_b92gVAuQFXh2nt6o'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
