import { Component, Input } from '@angular/core';

@Component({
  selector: 'sg-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.less']
})
export class PanelComponent {
  @Input()
  public label: string;

  @Input()
  public iconType: string;

}
